# Especialización en desarrollo FullStack
Desarrollo FullStack por la universidad Austral, cursado en Coursera.

## Diseñando páginas web con Bootstrap 4 - Evaluación del proyecto - Módulo 3

### Ejercicios
1. Crear comportamiento dinámico utilizando Javascript
2. Crear elementos de navegación utilizando pestañas y menús desplegables
3. Crear alertas y vistas modales y un carousel.

---

### Web sobre libros de programación
Una página web responsive sobre los mejores libros de programación y dónde poder adquirlos en España

### Tecnologías
- HTML / CSS / JS
- Bootstrap 4
- NodeJS
- Visual Studio Code
- Git
- Bitbucket